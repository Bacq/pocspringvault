Here are the commands to set up vault localy :

1) Start a vault under default uri (http:127.0.0.1:8200) with root token = 00000000-0000-0000-0000-000000000000
> vault server --dev --dev-root-token-id="00000000-0000-0000-0000-000000000000"

2) Set up local variable to use vault CLI 
> export export VAULT_TOKEN="00000000-0000-0000-0000-000000000000"
>
>  export VAULT_ADDR="http://127.0.0.1:8200"

3) Add 2 two secrets under application-name with CLI
> vault kv put secret/application-name example.username=vaultUsername example.password=vaultPassword

4) (OPTIONNAL) Get the 2 added secrets from application-name with CLI
> vault kv get secret/application-name
