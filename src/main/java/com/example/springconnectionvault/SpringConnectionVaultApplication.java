package com.example.springconnectionvault;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@EnableConfigurationProperties(VaultConfiguration.class)
public class SpringConnectionVaultApplication implements CommandLineRunner {

	private Logger log = LoggerFactory.getLogger(SpringConnectionVaultApplication.class);

	VaultConfiguration configuration;

	public SpringConnectionVaultApplication(VaultConfiguration configuration) {
		this.configuration = configuration;
	}

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(SpringConnectionVaultApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		log.info("username = {}", configuration.getUsername());
		log.info("password = {}", configuration.getPassword());
	}
}
