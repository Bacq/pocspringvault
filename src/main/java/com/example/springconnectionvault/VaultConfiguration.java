package com.example.springconnectionvault;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("example")
public class VaultConfiguration {

    private String username;
    private String password;

}
